export CARLA_MAPS_PATH=/opt/carla/HDMaps/
export CARLA_ROOT=/opt/carla
export CARLA_SERVER=/opt/carla/bin/CarlaUE4.sh
export CARLA_AUTOWARE_ROOT=/home/autoware/carla-autoware/autoware_launch
export PYTHONPATHCARLA="/opt/carla/PythonAPI/carla/:/opt/carla/PythonAPI/carla/agents/:/opt/carla/PythonAPI/carla/dist/carla-0.9.8-py2.7-linux-x86_64.egg"
export PYTHONPATH=$PYTHONPATH:/opt/carla/PythonAPI/carla/dist/carla-0.9.8-py2.7-linux-x86_64.egg